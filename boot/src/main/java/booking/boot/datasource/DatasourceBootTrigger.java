package booking.boot.datasource;

import lombok.experimental.UtilityClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.StandardEnvironment;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @UtilityClass annotation is use to define a class without constructor and only static methods
 * This class implement the mechanism of reach and retry to connect to the database
 */
@UtilityClass
public final class DatasourceBootTrigger {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatasourceBootTrigger.class);

    public static final void boot(String datasourceUrl, int maxAttempts, int interval) throws InterruptedException {
        LOGGER.debug("Resolving placeholders in datasource url : {}", datasourceUrl);
        datasourceUrl = new StandardEnvironment().resolvePlaceholders(datasourceUrl);
        LOGGER.debug("Resolved datasource url : {}", datasourceUrl);
        LOGGER.info("Attempting to connect to database");
        Connection finalConnection = null;
        int attempts = 0;
        while (finalConnection == null && attempts < maxAttempts) {
            try (Connection connection = DriverManager.getConnection(datasourceUrl)) {
                LOGGER.debug("Attempt {} to connect to database", attempts);
                finalConnection = connection;

            } catch (SQLException e) {
                LOGGER.warn("Attempt {} failed", attempts, e);
            } finally {
                attempts++;
                Thread.sleep(interval);
            }
        }


    }
}
