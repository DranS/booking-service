package booking.boot.datasource;

/**
 * Definition of an Exception for no reach the databse after all attempts
 */
public class DatasourceNotAvailableException extends RuntimeException {
    public DatasourceNotAvailableException(String message, Throwable cause) {
        super(message, cause);
    }
}
