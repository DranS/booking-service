package booking.boot.tenant;

import booking.infrastructure.tenant.Tenant;
import booking.infrastructure.tenant.TenantValue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * This class is load during the boot of the Web service to configure it with the @Configuration annotation
 * This class load from environment variable the "TenantId" which is the name of the instance of the booking service
 */
@Configuration
public class TenantConfiguration {

    @Value("${tenant.tenantId}")
    private String tenantId;

    @Bean
    public Tenant tenant(){
        return new TenantValue(tenantId);
    }
}
