/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package booking.boot.controller;


import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * This class is load during the boot of the Web service to configure it with the @Configuration annotation
 * This class initiate a mapper for Object @see <a href ="https://github.com/DozerMapper/dozer">Dozer Mapper</a>
 */
@Configuration
public class ClientConfiguration {

    @Bean
    public Mapper mapper() {
        return new DozerBeanMapper();
    }

}
