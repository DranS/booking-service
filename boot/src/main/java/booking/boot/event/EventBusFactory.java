package booking.boot.event;

import booking.infrastructure.eventbus.GuavaBookingEventListener;
import booking.infrastructure.tenant.Tenant;
import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.EventBus;
import booking.domain.common.event.Publisher;
import booking.domain.booking.event.BookingEventListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static java.util.concurrent.Executors.newCachedThreadPool;

/**
 * This class is load during the boot of the Web service to configure it with the @Configuration annotation
 * This class initiate and map the internal @see <a href ="https://github.com/google/guava/wiki/EventBusExplained">event bus Guava</a>
 * To the function of publish event outside of the service in the kafka message broker
 */
@Configuration
public class EventBusFactory {

    private static final EventBus asyncBus() {
        return new AsyncEventBus(newCachedThreadPool());
    }

    @Bean
    public EventBus bookingEventBus() {
        return asyncBus();
    }

    @Bean
    public BookingEventListener bookingEventListener(@Autowired @Qualifier("kafkaBookingLifecyclePublisher")
                                                                 Publisher kafkaBookingLifecyclePublisher,
                                                     @Autowired Tenant tenant,
                                                     @Autowired @Qualifier("bookingEventBus") EventBus bookingEventBus) {

        GuavaBookingEventListener bookingEventListener = new GuavaBookingEventListener(kafkaBookingLifecyclePublisher, tenant);

        bookingEventBus.register(bookingEventListener);
        return bookingEventListener;
    }

}
