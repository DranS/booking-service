package booking.boot.broker;

import booking.domain.common.event.Publisher;
import booking.infrastructure.broker.KafkaPublisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.KafkaTemplate;

/**
 * This class is load during the boot of the Web service to configure it with the @Configuration annotation
 * This class enable the use of Kafka Spring with the @EnableKafka annotation
 * This class instantiate class and inject them in @Bean (singleton) to be use with @Autowired annotation in all the service
 * This class initiate the Kafka publisher to publish in kafka message broker
 */
@Configuration
@EnableKafka
public class KafkaBrokerConfiguration {

    @Value("${spring.kafka.topic.domain.booking}")
    private String kafkaTopicBooking;

    @Bean
    public Publisher kafkaBookingLifecyclePublisher(@Autowired KafkaTemplate kafkaTemplate) {
        return new KafkaPublisher() {
            @Override
            protected KafkaTemplate getKafkaTemplate() {
                return kafkaTemplate;
            }

            @Override
            protected String getTopic() {
                return kafkaTopicBooking;
            }
        };
    }
}
