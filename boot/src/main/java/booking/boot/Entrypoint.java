package booking.boot;

import booking.boot.datasource.DatasourceNotAvailableException;
import booking.boot.datasource.DatasourceBootTrigger;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;


/**
 * This class is use to be load before the Web service boot to reach the connection to the database and
 * retry until the connection is not reach with max-attempts and interval time of retry.
 * The max-attempts and interval time of retry are retrieve from spring context environment.
 * The setting of this class is done in /boot/src/main/resources/META-INF/spring.factories
 */
public class Entrypoint implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    /*
    * Retrieve ENV variable from Spring Context
    */
    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        this.initializeDatasource(applicationContext.getEnvironment());
    }

    /*
    * Call function to establish database connection with retry mechanism
    */
    private void initializeDatasource(ConfigurableEnvironment environment) {
            try {
                String datasourceBootstrapUrl = environment.getProperty("spring.datasource.bootstrap.url");
                int maxConnectionAttempts = Integer.parseInt(environment.getProperty("spring.datasource.bootstrap.max-attempts"));
                int connectionInterval = Integer.parseInt(environment.getProperty("spring.datasource.bootstrap.interval"));
                DatasourceBootTrigger.boot(
                        datasourceBootstrapUrl,
                        maxConnectionAttempts,
                        connectionInterval);
            } catch (InterruptedException e) {
                throw new DatasourceNotAvailableException("Cannot initialize datasource", e);
            }
    }
}
