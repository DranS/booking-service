package booking.infrastructure.tenant;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Concrete class which expose the TenantId (Id of the instance of the booking service)
 */
@Data
@AllArgsConstructor
public class TenantValue implements Tenant {

    private String tenantId;
}
