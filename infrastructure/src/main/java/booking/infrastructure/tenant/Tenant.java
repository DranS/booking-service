package booking.infrastructure.tenant;

/**
 * Interface to expose the TenantId (Id of the instance of the booking service)
 */
public interface Tenant {

    String getTenantId();
}
