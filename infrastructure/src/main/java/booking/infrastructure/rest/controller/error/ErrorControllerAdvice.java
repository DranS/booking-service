package booking.infrastructure.rest.controller.error;

import booking.domain.booking.error.BookingAlreadyExist;
import booking.domain.common.time.error.TimeSlotInvalid;
import booking.domain.room.error.MeetingRoomNotFound;
import booking.domain.user.error.UserNotAuthorized;
import booking.domain.user.error.UserNotFound;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.IllegalFormatException;

/**
 * The annotation @ControllerAdvice means that ALL error/exception which are throw from use cases
 * and extends RuntimeException are catch and manage by the class
 * Then, the error / exception are convert to HTTP error to answer to the client
 */
@ControllerAdvice
public class ErrorControllerAdvice {

    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorControllerAdvice.class);

    @ExceptionHandler({MethodArgumentNotValidException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Error methodArgumentNotValidException(MethodArgumentNotValidException e) {
        LOGGER.warn("methodArgumentNotValidException : {}", e.getMessage(), e.getCause());
        return new Error(
                e.getBindingResult().getFieldErrors().stream()
                        .map(DefaultMessageSourceResolvable::getDefaultMessage)
                        .findFirst()
                        .orElse(e.getMessage())
        );
    }

    @ExceptionHandler({IllegalStateException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Error lllegalStateException(IllegalFormatException e) {
        LOGGER.warn("IllegalStateException : {}", e.getMessage(), e.getCause());
        return new Error(e.getMessage());
    }

    @ExceptionHandler({TimeSlotInvalid.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Error lllegalStateException(TimeSlotInvalid e) {
        LOGGER.warn("TimeSlotInvalid : {}", e.getMessage(), e.getCause());
        return new Error(e.getMessage());
    }

    @ExceptionHandler({DataAccessException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public Error dataAccessException(DataAccessException e) {
        LOGGER.warn("data Access Exception : {}", e.getMessage());
        return new Error(e.getMessage());
    }

    @ExceptionHandler({UserNotAuthorized.class})
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public Error lllegalStateException(UserNotAuthorized e) {
        LOGGER.warn("Not Authorized : {}", e.getMessage(), e.getCause());
        return new Error(e.getMessage());
    }

    @ExceptionHandler({BookingAlreadyExist.class})
    @ResponseStatus(HttpStatus.CONFLICT)
    @ResponseBody
    public Error bookingAlreadyExist(BookingAlreadyExist e) {
        LOGGER.warn("The booking is already exist : {}", e.getMessage());
        return new Error(e.getMessage());
    }

    @ExceptionHandler({UserNotFound.class})
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public Error userNotFound(UserNotFound e) {
        LOGGER.warn("The user is not found : {}", e.getMessage());
        return new Error(e.getMessage());
    }

    @ExceptionHandler({MeetingRoomNotFound.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public Error meetingRoomNotFound(MeetingRoomNotFound e) {
        LOGGER.warn("The user is not found : {}", e.getMessage());
        return new Error(e.getMessage());
    }

}
