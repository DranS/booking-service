package booking.infrastructure.rest.controller.validator.uuid;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Definition of the regex to validate a UUID format
 */
public class UUIDValidator implements ConstraintValidator<UUID, String> {


    @Override
    public void initialize(UUID constraintAnnotation) {
        // Do nothing because of ConstraintValidator compliance
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value.toLowerCase().matches("[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[34][0-9a-fA-F]{3}-[89ab][0-9a-fA-F]{3}-[0-9a-fA-F]{12}");
    }
}
