package booking.infrastructure.rest.controller;

import booking.domain.booking.command.CancelBookingCommand;
import booking.domain.booking.command.CreateBookingCommand;
import booking.infrastructure.rest.controller.model.BookingRequest;
import booking.infrastructure.rest.controller.model.CancelBookingRequest;
import booking.usecase.booking.BookingService;
import booking.usecase.cancel.CancelService;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.ACCEPTED;
import static org.springframework.http.HttpStatus.CREATED;

/**
 * Controller of the Web service
 */
@RestController
@RequestMapping(Mapping.Booking.ROOT_BOOKING_V1)
public class BookingController {
    private static final Logger LOGGER = LoggerFactory.getLogger(BookingController.class);

    @Autowired
    private Mapper mapper;

    @Autowired
    private BookingService bookingService;
    @Autowired
    private CancelService cancelService;

    @PostMapping(Mapping.Booking.BOOK)
    @ResponseStatus(CREATED)
    public void book(@Valid @RequestBody BookingRequest bookingRequest) {
        LOGGER.info("Entering book controller with args : {}", bookingRequest);

        CreateBookingCommand bookingCommand = mapper.map(bookingRequest, CreateBookingCommand.class);

        bookingCommand.getTimeSlot().validate();

        bookingService.booking(bookingCommand);
    }

    @PostMapping(Mapping.Booking.CANCEL)
    @ResponseStatus(ACCEPTED)
    public void cancel(@Valid @RequestBody CancelBookingRequest cancelBookingRequest) {
        LOGGER.info("Entering cancel booking with args : {}", cancelBookingRequest);

        CancelBookingCommand cancelBookingCommand = mapper.map(cancelBookingRequest, CancelBookingCommand.class);

        cancelBookingCommand.getTimeSlot().validate();

        cancelService.cancelBooking(cancelBookingCommand);

    }

}
