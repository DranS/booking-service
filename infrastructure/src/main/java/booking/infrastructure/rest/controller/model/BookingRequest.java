package booking.infrastructure.rest.controller.model;

import booking.infrastructure.rest.controller.validator.user.UserEmailValidation;
import booking.domain.common.time.TimeSlot;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * Definition of the request object who represent the API JSON request from Controller
 */
@Data
public class BookingRequest {

    @NotNull
    @UserEmailValidation
    private String userEmail;
    @NotNull
    private String meetingRoomName;
    @NotNull
    private TimeSlot timeSlot;
}
