package booking.infrastructure.rest.controller.validator.user;

import booking.domain.user.UserEmail;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Definition of the regex to validate a Email address format
 */
public class UserEmailValidator implements ConstraintValidator<UserEmailValidation, String> {

    @Override
    public void initialize(UserEmailValidation constraintAnnotation) {
        // Do nothing because of ConstraintValidator compliance
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return UserEmail.isValidEmailAddress(value);
    }

}
