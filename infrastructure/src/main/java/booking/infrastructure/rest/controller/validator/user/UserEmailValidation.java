package booking.infrastructure.rest.controller.validator.user;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Annotation definition to validate an Email address
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {UserEmailValidator.class})
public @interface UserEmailValidation {
    String message() default "Email value is not valid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}