/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package booking.infrastructure.rest.controller;

import lombok.experimental.UtilityClass;

/**
 * Class to centralize the definition of URI of all controller
 */
public final class Mapping {

    private Mapping() {

    }

    @UtilityClass
    public static final class Booking {

        public static final String  ROOT_BOOKING_V1 = "/book-manager";

        public static final String BOOK = "/book";

        public static final String CANCEL = "/cancel";

    }

}
