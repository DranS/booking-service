package booking.infrastructure.rest.controller.error;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Definition of the structure of Error which can be return by the Controller
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Error {
    private String errorMessage;
}
