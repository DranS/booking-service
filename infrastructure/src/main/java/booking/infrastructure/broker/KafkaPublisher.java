/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package booking.infrastructure.broker;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import booking.domain.common.event.DomainEvent;
import booking.domain.common.event.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.kafka.core.KafkaTemplate;

/**
 * Abstract class who implement the Publish method to write Event into a message broker kafka
 */
@Qualifier
public abstract class KafkaPublisher implements Publisher {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaPublisher.class);

    private final Gson gson = new GsonBuilder().setPrettyPrinting().create();

    protected abstract KafkaTemplate getKafkaTemplate();

    protected abstract String getTopic();

    @Override
    public void publish(DomainEvent event) {
        LOGGER.debug("Publishing with KafkaPublisher");

        String eventJson = gson.toJson(event);
        LOGGER.info("Event Message {}", eventJson);

        getKafkaTemplate().send(getTopic(), eventJson);
        LOGGER.debug("Event successfully published {}", event.getRepresentation());
    }


}
