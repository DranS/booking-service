package booking.infrastructure.eventbus;

import booking.infrastructure.tenant.Tenant;
import com.google.common.eventbus.Subscribe;
import booking.domain.booking.event.BookingEvent;
import booking.domain.common.event.Publisher;
import booking.domain.booking.event.BookingEventListener;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Concrete class is load during the boot of the Web service to configure it with the @Configuration annotation
 * Concrete class which implement the apply method to Handle Booking Event
 * The Event is transmitted from Guava EventBus @see <a href ="https://github.com/google/guava/wiki/EventBusExplained">event bus Guava</a>
 */
@AllArgsConstructor
public class GuavaBookingEventListener implements BookingEventListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(GuavaBookingEventListener.class);

    private Publisher kafkaBookingLifecyclePublisher;
    private Tenant tenant;

    @Subscribe
    @Override
    public void apply(BookingEvent bookingEvent) {
        LOGGER.info("Applying GuavaBookingEventListener");
        bookingEvent.setTenantId(tenant.getTenantId());

        kafkaBookingLifecyclePublisher.publish(bookingEvent);
    }

}
