package booking.infrastructure.repository;

import booking.domain.user.User;
import booking.domain.user.UserEmail;
import booking.domain.user.UserRepository;
import booking.infrastructure.repository.mapper.UserMapper;
import lombok.Getter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Concrete class which implement the Interface of repository define in the "Domain" module
 * The concrete repository access to the data through an relational database management system (RDBMS)
 */
@Repository
public class JdbcUserRepository extends JdbcRepository implements UserRepository {

    private static final String FIND_BY_EMAIL = "SELECT * from `user` where email = ?";

    @Getter
    private final RowMapper<User> mapper = new UserMapper();


    @Override
    public Optional<User> findByEmail(UserEmail email) {
        return queryOne(
                FIND_BY_EMAIL,
                email.getEmail()
        );
    }

}
