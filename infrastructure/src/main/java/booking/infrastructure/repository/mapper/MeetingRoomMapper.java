package booking.infrastructure.repository.mapper;

import booking.domain.common.uuid.ValueObjectUUID;
import booking.domain.room.MeetingRoom;
import booking.domain.room.MeetingRoomId;
import booking.domain.room.MeetingRoomName;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Mapper to convert data extract from database to Object
 */
public class MeetingRoomMapper implements RowMapper<MeetingRoom> {

    @Override
    public MeetingRoom mapRow(ResultSet rs, int rowNum) throws SQLException {

        MeetingRoomId meetingRoomId = new MeetingRoomId(
                ValueObjectUUID.getUidFromByteArray(rs.getBytes("meeting_room_id"))
        );

        MeetingRoomName name = new MeetingRoomName(rs.getString("meeting_room_name"));

        return MeetingRoom.builder()
                .meetingRoomId(meetingRoomId)
                .meetingRoomName(name)
                .info(rs.getString("info"))
                .build();
    }

}
