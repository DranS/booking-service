package booking.infrastructure.repository.mapper;

import booking.domain.booking.Booking;
import booking.domain.booking.BookingId;
import booking.domain.common.time.TimeSlot;
import booking.domain.common.uuid.ValueObjectUUID;
import booking.domain.room.MeetingRoomId;
import booking.domain.user.UserId;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Mapper to convert data extract from database to Object
 */
public class BookingMapper implements RowMapper<Booking> {

    @Override
    public Booking mapRow(ResultSet rs, int rowNum) throws SQLException {
        BookingId bookingId = new BookingId(
                ValueObjectUUID.getUidFromByteArray(rs.getBytes("booking_id"))
        );

        MeetingRoomId meetingRoomId = new MeetingRoomId(
                ValueObjectUUID.getUidFromByteArray(rs.getBytes("meeting_room_id"))
        );

        UserId userId = new UserId(
                ValueObjectUUID.getUidFromByteArray(rs.getBytes("user_id"))
        );

        TimeSlot timeSlot = new TimeSlot(
                rs.getInt("starting_hour"),
                rs.getInt("ending_hour")
        );

        return Booking.builder()
                .bookingId(bookingId)
                .meetingRoomId(meetingRoomId)
                .userId(userId)
                .timeSlot(timeSlot)
                .build();
    }

}
