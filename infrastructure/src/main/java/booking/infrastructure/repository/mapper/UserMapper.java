package booking.infrastructure.repository.mapper;

import booking.domain.common.uuid.ValueObjectUUID;
import booking.domain.user.User;
import booking.domain.user.UserEmail;
import booking.domain.user.UserId;
import booking.domain.user.UserName;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Mapper to convert data extract from database to Object
 */
public class UserMapper implements RowMapper<User> {

    @Override
    public User mapRow(ResultSet rs, int rowNum) throws SQLException {

        UserId userId = new UserId(
                ValueObjectUUID.getUidFromByteArray(rs.getBytes("user_id"))
        );

        UserEmail email = new UserEmail(rs.getString("email"));

        UserName name = new UserName(rs.getString("name"));

        return User.builder()
                .userId(userId)
                .userEmail(email)
                .name(name)
                .build();
    }

}
