package booking.infrastructure.repository;

import booking.domain.booking.Booking;
import booking.domain.booking.BookingRepository;
import booking.domain.common.time.TimeSlot;
import booking.domain.room.MeetingRoomId;
import booking.infrastructure.repository.mapper.BookingMapper;
import lombok.Getter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Concrete class which implement the Interface of repository define in the "Domain" module
 * The concrete repository access to the data through an relational database management system (RDBMS)
 */
@Repository
public class JdbcBookingRepository extends JdbcRepository implements BookingRepository {

    private static final String FIND_BY_MEETING_ROOM_ID_AND_TIME_SLOT =
            "SELECT * from `booking` where meeting_room_id = ? and starting_hour = ? and ending_hour = ?";

    private static final String SAVE_QUERY = "INSERT INTO `booking`(booking_id, starting_hour, ending_hour, meeting_room_id, user_id) VALUES(?,?,?,?,?)";
    private static final String DELETE_QUERY = "DELETE FROM `booking` WHERE booking_id = ?";



    @Getter
    private final RowMapper<Booking> mapper = new BookingMapper();

    @Override
    public Optional<Booking> findByMeetingRoomIdAndTimeSlot(MeetingRoomId meetingRoomId, TimeSlot timeSlot) {
        return queryOne(
                FIND_BY_MEETING_ROOM_ID_AND_TIME_SLOT,
                meetingRoomId.bytes(),
                timeSlot.getStartingHour(),
                timeSlot.getEndingHour()
        );
    }

    @Override
    public void save(Booking booking) {
        jdbcTemplate.update(SAVE_QUERY,
                booking.getBookingId().bytes(),
                booking.getTimeSlot().getStartingHour(),
                booking.getTimeSlot().getEndingHour(),
                booking.getMeetingRoomId().bytes(),
                booking.getUserId().bytes()
        );
    }

    @Override
    public void delete(Booking booking) {

        jdbcTemplate.update(DELETE_QUERY,
                booking.getBookingId().bytes()
        );
    }
}
