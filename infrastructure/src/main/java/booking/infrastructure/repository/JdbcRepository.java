package booking.infrastructure.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.util.List;
import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;

/**
 * Generic abstract class which implement method to request (SELECT, INSERT, DELETE) query
 */
public abstract class JdbcRepository {
    @Autowired
    protected JdbcTemplate jdbcTemplate;

    protected <T> Optional<List<T>> query(String sql, Object... args) {
        return query(sql, getMapper(), args);
    }

    // Manage the DataAccessException with HTTP 500 => INTERNAL_SERVER_ERROR
    protected <T> Optional<List<T>> query(String sql, RowMapper<T> mapper, Object... args) {
        try {
            return ofNullable(
                    jdbcTemplate.query(sql, mapper, args)
            );
        } catch (EmptyResultDataAccessException e) {
            return empty();
        }
    }

    protected <T> Optional<T> queryOne(String sql, Object... args) {
        return queryOne(sql, getMapper(), args);
    }

    // Manage the DataAccessException with HTTP 500 => INTERNAL_SERVER_ERROR
    protected <T> Optional<T> queryOne(String sql, RowMapper<T> mapper, Object... args) {
        try {
            return ofNullable(
                    jdbcTemplate.queryForObject(sql, mapper, args)
            );
        } catch (EmptyResultDataAccessException e) {
            return empty();
        }
    }

    protected abstract <T> RowMapper<T> getMapper();
}