package booking.infrastructure.repository;

import booking.infrastructure.repository.mapper.MeetingRoomMapper;
import booking.domain.room.MeetingRoom;
import booking.domain.room.MeetingRoomRepository;
import lombok.Getter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Concrete class which implement the Interface of repository define in the "Domain" module
 * The concrete repository access to the data through an relational database management system (RDBMS)
 */
@Repository
public class JdbcMeetingRoomRepository extends JdbcRepository implements MeetingRoomRepository {

    private static final String FIND_BY_NAME = "SELECT * from `meeting-room` where meeting_room_name = ?";

    @Getter
    private final RowMapper<MeetingRoom> mapper = new MeetingRoomMapper();

    @Override
    public Optional<MeetingRoom> findByMeetingRoomName(String name) {
        return queryOne(
                FIND_BY_NAME,
                name
        );
    }

}
