# Booking service

The booking-service manage the request (**command**) to book and cancel a meeting room for an employee. 

##### Booking a meeting room

To book a meeting room, the `client` need to call the URI `/book-manager/book` with a JSON like :

```json
{
	"userEmail": "ACG21CNQFM@pepsi.com",
	"meetingRoomName": "P01",
	"timeSlot": {
		"startingHour": 2,
		"endingHour": 3
	}
}
```

The controls make by the booking service are :

* The booking service authenticates the user from an authentication service (like an LDAP server, etc...) 
* The booking service controls that the meeting-room exist
* Verify if a booking already exists for this time slot of the meeting-room 
  - **If not**, the booking is saved in database, an event `BookingUpdatedEvent` is published in the message broker with a field `bookingState:CREATED`  and an `201 HTTP` is returned
  - **If yes**, booking service will return a Conflict code  `409 HTTP` 
* The event `BookingUpdatedEvent` is read by the [consulting service](https://gitlab.com/DranS/consulting-service) from [Kafka](http://kafka.apache.org/). The consulting service saves in his database the new booking with all his information. 



##### Cancel a meeting room

To cancel a meeting room, the `client` need to call the URI `/book-manager/cancel` with a JSON like :

```json
{
	"userEmail": "ACG21CNQFM@pepsi.com",
	"meetingRoomName": "P01",
	"timeSlot": {
		"startingHour": 2,
		"endingHour": 3
	}
}
```

- The booking service authenticates the user from an authentication service (like an LDAP server, etc...) 
- The booking service controls that the meeting-room exist
- The booking service verifies if a booking already exists for this time slot of the meeting-room by this user
  - **If yes**, the booking is deleted from database, an event `BookingUpdatedEvent` is published in the message broker with a field `bookingState:DELETED`  and an `202 HTTP` is returned
  - **If not**, booking service will return an Accepted code  `202 HTTP`  (to be fault tolerant)
- The event `BookingUpdatedEvent` is read by the [consulting service](https://gitlab.com/DranS/consulting-service) from [Kafka](http://kafka.apache.org/). The consulting service deletes this booking from his database. 



## Technical characteristic

The booking service was developed, with in mind, to be **a generic booking service** without any specification or dependency of the company for which he will be instantiated.

All information dependent on the company is given by **environment variables**. 

Environment variables are :

```yaml
- name: TENANT_ID			# Identifier of the company (COKE, PEPSI)
- name: BROKER_KAFKA_HOST	 # URL and PORT of the message broker kafka
- name: DB_HOST				# URL of the database 
- name: DB_PORT				# PORT of the database 
- name: DB_NAME				# Name of the database schema (COKE-booking-manager, PEPSI-booking-manager)
- name: DB_USER				# Username to connect to the database
- name: DB_PASSWORD			# Password to connect to the database
- name: LOG_LEVEL			# Level of LOGs (DEBUG, INFO, ERROR, etc...)
```



For example, for the [booking solution](https://gitlab.com/DranS/booking-solution), the companies (COKE and PEPSI) do not trust each other or anyone else. To respond to it, I decide to create an **instance of the booking service** for each company to segregate the control and the knowledge of meeting rooms and employees. 

To do it, I just have to define the  **environment variables** for each company without modifying any code line of the booking service. 

Environment variables for **COKE** :

```yaml
- name: TENANT_ID
	value: COKE
- name: DB_NAME
	value: COKE-booking-manager
- name: BROKER_KAFKA_HOST
	value: kafka:9092
- name: DB_HOST
	value: db
- name: DB_PORT
	value: "3306"	
- name: DB_USER
	value: root
- name: DB_PASSWORD
	value: admin	
- name: LOG_LEVEL
	value: INFO
```



Environment variables for **PEPSI** :

```yaml
- name: TENANT_ID
	value: PEPSI
- name: DB_NAME
	value: PEPSI-booking-manager
- name: BROKER_KAFKA_HOST
	value: kafka:9092
- name: DB_HOST
	value: db
- name: DB_PORT
	value: "3306"	
- name: DB_USER
	value: root
- name: DB_PASSWORD
	value: admin	
- name: LOG_LEVEL
	value: INFO
```



## Build Booking service

The booking service is thought to be built and deploy with `Docker `. The Docker file is :

```dockerfile
# build stage
FROM gradle:alpine AS build-env
USER root
RUN mkdir app
ADD . /app
WORKDIR /app
RUN gradle assemble --stacktrace --info

# final stage
FROM openjdk:8-jre
COPY --from=build-env /app/boot/build/libs/booking-service.jar app.jar
ENTRYPOINT [ "sh", "-c", "java -Djava.security.egd=file:/dev/./urandom -jar app.jar --server.port=8080" ]
EXPOSE 8080


```

#### Requirement

Before building the booking service, you need to :

- Have **Docker** (or a **Docker Machine**) on your system with access to the `docker` command



#### Build

To build the booking service :

```shell
docker build -t booking-service .
```



#### Publish docker image

##### Requirement

You need to be login to your docker registry to push the image on it `docker login $YOU_REGISTRY`



##### Publish

Before publishing, you need to define 2 arguments :

* The `tag` argument is the apply tag of all docker images
* The  `registry` argument is the repository to push all docker images

To publish the docker image, you need to:

```shell
docker tag booking-service $registry/booking-service:$tag
docker push $registry/booking-service:$tag
```

