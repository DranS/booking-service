/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package booking.usecase.booking;

import booking.domain.booking.Booking;
import booking.domain.booking.BookingRepository;
import booking.domain.booking.BookingState;
import booking.domain.booking.command.BookingCommand;
import booking.domain.booking.command.CreateBookingCommand;
import booking.domain.booking.error.BookingAlreadyExist;
import booking.domain.booking.event.BookingUpdatedEvent;
import booking.domain.room.MeetingRoom;
import booking.domain.user.User;
import booking.usecase.services.BookingProcessor;
import booking.usecase.services.ControlBookingService;
import com.google.common.eventbus.EventBus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Concrete Service to apply the Booking service
 * This class implement the Interface Injection to use the Inversion of control Design Pattern
 * @see <a href="https://martinfowler.com/articles/injection.html">Inversion of control Design Pattern</a>
 */
@Service
public class BookingService implements BookingProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookingService.class);

    @Autowired
    protected BookingRepository bookingRepository;
    @Autowired
    private ControlBookingService controlBookingService;

    @Autowired
    private EventBus bookingEventBus;


    public void booking(CreateBookingCommand createBookingCommand) {

        LOGGER.info("Entering booking service with args : {}", createBookingCommand);

        controlBookingService.process(
                createBookingCommand,
                this
        );
    }

    @Override
    public void process(BookingCommand command, MeetingRoom room, User user) {

        // check if the meeting room is book for this Time Slot
        Optional<Booking> availableBooking = bookingRepository.findByMeetingRoomIdAndTimeSlot(room.getMeetingRoomId(), command.getTimeSlot());

        if (!availableBooking.isPresent()) {
            Booking booking = new Booking(
                    command.getTimeSlot(),
                    room.getMeetingRoomId(),
                    user.getUserId()
            );
            saveAndPublish(booking, room, user);
        } else {
            // Generate an Exception which return HTTP 409 => Conflict
            throw new BookingAlreadyExist(availableBooking.get().getBookingId());
        }
    }

    private void saveAndPublish(Booking booking, MeetingRoom room, User user) {
        LOGGER.info("Saving booking instance");
        bookingRepository.save(booking);

        LOGGER.info("Publish booking instance");
        bookingEventBus.post(new BookingUpdatedEvent(booking, BookingState.CREATED, room, user));
    }
}
