package booking.usecase.services;

import booking.domain.booking.command.BookingCommand;
import booking.domain.room.MeetingRoom;
import booking.domain.user.User;

/**
 * Interface for Interface Injection to apply Inversion of control Design Pattern
 * IOC : @see <a href="https://martinfowler.com/articles/injection.html">Inversion of control Design Pattern</a>
 *
 */
public interface BookingProcessor {

    void process(BookingCommand command, MeetingRoom room, User user);
}
