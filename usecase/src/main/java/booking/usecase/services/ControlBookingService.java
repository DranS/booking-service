package booking.usecase.services;

import booking.domain.booking.command.BookingCommand;
import booking.domain.room.MeetingRoom;
import booking.domain.room.MeetingRoomRepository;
import booking.domain.room.error.MeetingRoomNotFound;
import booking.domain.user.User;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Common class of control and validation for Booking and Cancel service
 */
@Service
@Data
public class ControlBookingService {

    @Autowired
    protected MeetingRoomRepository meetingRoomRepository;

    @Autowired
    protected Authentication authentication;


    public void process(BookingCommand command, BookingProcessor processor) {
        // check if the user existing with the authentication service
        // Generate an Exception which return HTTP 401 => Unauthorized
        User user = authentication.verifyUser(command.getUserEmail());

        // check the existing the meeting rooms
        Optional<MeetingRoom> optionalMeetingRoom = meetingRoomRepository.findByMeetingRoomName(command.getMeetingRoomName());
        MeetingRoom meetingRoom = optionalMeetingRoom.orElseThrow(()
                // Generate an Exception which return HTTP 404 => Not Found
                -> new MeetingRoomNotFound(command.getMeetingRoomName())
        );
        processor.process(command, meetingRoom, user);
    }


}
