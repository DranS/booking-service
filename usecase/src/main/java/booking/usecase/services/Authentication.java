package booking.usecase.services;

import booking.domain.user.User;
import booking.domain.user.UserEmail;
import booking.domain.user.UserRepository;
import booking.domain.user.error.UserNotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;


/**
 * Service use to find and authenticate the user who call the service.
 * We can imagine an improvement of this service by authenticate from an LDAP server
 */
@Service
public class Authentication {

    @Autowired
    private UserRepository userRepository;

    public User verifyUser(String email){
        UserEmail userEmail = new UserEmail(email);

        Optional<User> optionalUser = userRepository.findByEmail(userEmail);

        User user = optionalUser.orElseThrow(()
                -> new UserNotFound(userEmail)
        );

        return user;
    }
}
