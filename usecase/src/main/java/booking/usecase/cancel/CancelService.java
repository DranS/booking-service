/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package booking.usecase.cancel;

import booking.domain.user.error.UserNotAuthorized;
import com.google.common.eventbus.EventBus;
import booking.domain.booking.Booking;
import booking.domain.booking.BookingRepository;
import booking.domain.booking.BookingState;
import booking.domain.booking.command.BookingCommand;
import booking.domain.booking.command.CancelBookingCommand;
import booking.domain.booking.event.BookingUpdatedEvent;
import booking.domain.room.MeetingRoom;
import booking.domain.user.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import booking.usecase.services.BookingProcessor;
import booking.usecase.services.ControlBookingService;

import java.util.Optional;

/**
 * Concrete Service to apply the Cancel service
 * This class implement the Interface Injection to use the Inversion of control Design Pattern
 * @see <a href="https://martinfowler.com/articles/injection.html">Inversion of control Design Pattern</a>
 */
@Service
public class CancelService implements BookingProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(CancelService.class);

    @Autowired
    private ControlBookingService controlBookingService;
    @Autowired
    private BookingRepository bookingRepository;
    @Autowired
    private EventBus bookingEventBus;


    public void cancelBooking(CancelBookingCommand cancelBookingCommand) {
        LOGGER.info("Entering cancel booking service with args : {}", cancelBookingCommand);

        controlBookingService.process(
                cancelBookingCommand,
                this
                );
    }

    @Override
    public void process(BookingCommand command, MeetingRoom room, User user) {
        Optional<Booking> availableBooking = bookingRepository.findByMeetingRoomIdAndTimeSlot(
                room.getMeetingRoomId(),
                command.getTimeSlot()
        );

        // Delete the booking if he is exist
        availableBooking.ifPresent(booking -> deleteAndPublish(booking, room, user));
    }

    private void deleteAndPublish(Booking booking, MeetingRoom room, User user){
        if(!booking.getUserId().equals(user.getUserId()))
            throw new UserNotAuthorized(user.getUserEmail());

        bookingRepository.delete(booking);

        bookingEventBus.post(new BookingUpdatedEvent(booking, BookingState.DELETED, room, user));
    }
}
