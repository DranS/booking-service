package booking.domain.booking.error;


import booking.domain.booking.BookingId;

/**
 * This class define an error /Exception can occur if a Business Rule is not respected
 */
public class BookingAlreadyExist extends RuntimeException {

    private static final String MESSAGE_TEMPLATE = "Booking identified by %s already exist";

    public BookingAlreadyExist(BookingId bookingId) {
        super(String.format(MESSAGE_TEMPLATE, bookingId.getUuid().toString()));
    }

}