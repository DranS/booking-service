package booking.domain.booking;


import booking.domain.common.time.TimeSlot;
import booking.domain.room.MeetingRoomId;

import java.util.Optional;

/**
 * Interface which define method use in different usecase
 * This interface can be use to easily change the implementation of the repository without to change the usecase
 */
public interface BookingRepository {

    Optional<Booking> findByMeetingRoomIdAndTimeSlot(MeetingRoomId meetingRoomId, TimeSlot timeSlot);

    void save(Booking booking);

    void delete(Booking booking);

}
