package booking.domain.booking;


import booking.domain.room.MeetingRoomId;
import booking.domain.user.UserId;
import booking.domain.common.time.TimeSlot;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * Aggregate class which define a Booking object
 */
@AllArgsConstructor
@Data
@Builder
public class Booking {
    public static final String AGGREGATE_TYPE = Booking.class.getSimpleName();

    private BookingId bookingId;

    private TimeSlot timeSlot;

    private MeetingRoomId meetingRoomId;

    private UserId userId;

    public Booking(TimeSlot timeSlot, MeetingRoomId meetingRoomId, UserId userId) {
        this.timeSlot = timeSlot;
        this.meetingRoomId = meetingRoomId;
        this.userId = userId;
        this.bookingId = new BookingId();
    }

}


