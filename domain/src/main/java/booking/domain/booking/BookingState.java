package booking.domain.booking;

/**
 * Enum of states can have a booking object
 */
public enum BookingState {
    CREATED("CREATED"), UPDATED("UPDATED"), DELETED("DELETED");

    private String value;

    BookingState(String value) {
        this.value = value;
    }

}
