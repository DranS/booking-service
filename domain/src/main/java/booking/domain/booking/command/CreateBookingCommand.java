package booking.domain.booking.command;

import booking.domain.common.time.TimeSlot;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Concrete class which define the Command to book a meeting room and his arguments
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateBookingCommand implements BookingCommand {

    private String userEmail;
    private String meetingRoomName;
    private TimeSlot timeSlot;

}
