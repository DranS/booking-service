package booking.domain.booking.command;

import booking.domain.common.time.TimeSlot;

/**
 * Interface to define the base of all the Booking command use by the service
 */
public interface BookingCommand {

    String getUserEmail();
    String getMeetingRoomName();
    TimeSlot getTimeSlot();
}
