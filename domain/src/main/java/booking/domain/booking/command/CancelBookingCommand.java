package booking.domain.booking.command;

import booking.domain.common.time.TimeSlot;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Concrete class which define the Command to cancel a meeting room and his arguments
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CancelBookingCommand implements BookingCommand{

    private String userEmail;
    private String meetingRoomName;
    private TimeSlot timeSlot;

}
