package booking.domain.booking.event;

import booking.domain.booking.Booking;
import booking.domain.booking.BookingState;
import booking.domain.common.event.AbstractDomainEvent;
import booking.domain.room.MeetingRoom;
import booking.domain.user.User;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Concrete class which define the Event to publish outside to broadcast the change os state of a Booking aggregate
 */
@Data
@NoArgsConstructor
public class BookingUpdatedEvent extends AbstractDomainEvent implements BookingEvent {

    private Booking booking;
    private User user;
    private MeetingRoom meetingRoom;
    private BookingState bookingState;

    public BookingUpdatedEvent(Booking booking, BookingState bookingState, MeetingRoom meetingRoom, User user) {
        super(BookingUpdatedEvent.class.getSimpleName());
        this.booking = booking;
        super.correlationId = booking.getBookingId().toString();
        this.bookingState = bookingState;
        this.meetingRoom = meetingRoom;
        this.user = user;
    }
}
