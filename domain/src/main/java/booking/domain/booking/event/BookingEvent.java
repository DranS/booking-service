package booking.domain.booking.event;

import booking.domain.common.event.DomainEvent;

/**
 * Interface to define the base of all the Event link to the booking aggregate use by the service
 */
public interface BookingEvent extends DomainEvent {
}
