package booking.domain.booking.event;

/**
 * Interface which define method use to apply when receive a Booking Event
 * This interface can be use to easily change the implementation of the repository without to change the usecase
 */
public interface BookingEventListener {

    void apply(BookingEvent bookingEvent);
}
