package booking.domain.booking;


import booking.domain.common.uuid.ValueObjectUUID;

import java.io.Serializable;
import java.util.UUID;

/**
 * ValueObject class which define a BookingId object
 */
public final class BookingId extends ValueObjectUUID implements Serializable {

    public BookingId() {
        super();
    }

    public BookingId(String uuid) {
        super(uuid);
    }

    public BookingId(UUID uuid) {
        super(uuid);
    }
}
