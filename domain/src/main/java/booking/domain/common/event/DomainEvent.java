package booking.domain.common.event;

import java.util.UUID;

/**
 * Interface to define the base of all the Event use by the service
 */
public interface DomainEvent {

    UUID getUuid();

    String getRepresentation();

    String getType();

    String getCorrelationId();

    String getTenantId();

    void setTenantId(String tenantId);

}
