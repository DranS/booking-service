package booking.domain.common.event;

/**
 * Interface to define the method to publish an Event
 */
public interface Publisher {

    void publish(DomainEvent event);
}
