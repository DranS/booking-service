package booking.domain.common.event;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

import static java.util.UUID.randomUUID;

/**
 * An abstract class which define common variable of all Event
 */
@Data
@NoArgsConstructor
public class AbstractDomainEvent implements DomainEvent {
    protected UUID uuid;
    protected String type;
    protected String correlationId;
    protected String tenantId;

    protected AbstractDomainEvent(String type) {
        this(randomUUID(), type);
    }

    protected AbstractDomainEvent(UUID uuid, String type) {
        this.uuid = uuid;
        this.type = type;
    }

    @Override
    public String getRepresentation() {
        return String.format("[ %s ] : ( %s ) ", uuid.toString(), type);
    }

}
