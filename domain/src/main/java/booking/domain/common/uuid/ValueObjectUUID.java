package booking.domain.common.uuid;

import lombok.Getter;

import java.nio.ByteBuffer;
import java.util.Objects;
import java.util.UUID;

import static java.util.UUID.randomUUID;

/**
 * Definition of an abstract class to give access of UUID method and operations
 */
public abstract class ValueObjectUUID {

    @Getter
    private UUID uuid;

    public ValueObjectUUID() {
        this(randomUUID());
    }

    public ValueObjectUUID(String uuid) {
        this(UUID.fromString(uuid));
    }

    public ValueObjectUUID(UUID uuid) {
        Objects.requireNonNull(uuid, String.format("field uuid of %s  object required", this.getClass().getName()));
        this.uuid = uuid;
    }

    @Override
    public String toString() {
        return uuid.toString();
    }

    @Override
    public boolean equals(Object other) {
        if (uuid == null || other == null
                || this.getClass() != other.getClass()
                || !other.getClass().isAssignableFrom(this.getClass())) {
            return false;
        }
        return this.uuid.equals(((ValueObjectUUID) other).uuid);
    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }


    public byte[] bytes() {
        return ByteBuffer.allocate(16).putLong(uuid.getMostSignificantBits()).putLong(uuid.getLeastSignificantBits()).array();
    }

    public static UUID getUidFromByteArray(byte[] bytes) {
        ByteBuffer bb = ByteBuffer.wrap(bytes);
        long high = bb.getLong();
        long low = bb.getLong();
        return new UUID(high, low);
    }
}

