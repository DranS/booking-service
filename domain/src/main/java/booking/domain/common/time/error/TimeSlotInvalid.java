package booking.domain.common.time.error;


/**
 * This class define an error /Exception can occur if a Business Rule is not respected
 */
public class TimeSlotInvalid extends RuntimeException {

    private static final String MESSAGE_TEMPLATE = "The Time Slot with the starting hour %d and ending hour %d are not valid";

    public TimeSlotInvalid(Integer startingHour, Integer endingHour) {
        super(String.format(MESSAGE_TEMPLATE, startingHour.intValue(), endingHour.intValue()));
    }

}