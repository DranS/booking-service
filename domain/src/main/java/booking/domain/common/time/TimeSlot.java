package booking.domain.common.time;

import booking.domain.common.time.error.TimeSlotInvalid;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *  ValueObject class which define a Time Slot object
 *  The choice was to use Integer to represent the starting and ending Hour of the booking.
 *  We use 2 Integer (start and end) to give the ability to easily evolve the Business Rules (can book more than 1 hours for example)
 */
@Data
@NoArgsConstructor
public class TimeSlot {

    private Integer startingHour;
    private Integer endingHour;

    public TimeSlot(Integer startingHour, Integer endingHour) {
        if(!isValid(startingHour, endingHour))
            throw new TimeSlotInvalid(startingHour, endingHour);

        this.startingHour = startingHour;
        this.endingHour = endingHour;
    }

    /**
     * To validate an external TimeSlot object already created
     */
    public void validate(){
        if(!isValid(startingHour, endingHour))
            throw new TimeSlotInvalid(startingHour, endingHour);
    }

    /**
     * Business rule : Users can book meeting rooms by the hour. It's mean the Time Slot can be only one hour.
     * Business rule : The startingHour must be less than the endingHour
     * Business rule : control that the starting Hours are between 00 and 23 (00 a.m. and 11 p.m.)
     * Business rule : control that the ending Hours are between 01 and 24 (01 a.m. and 12 p.m.)
     */
    private boolean isValid(Integer startingHour, Integer endingHour){
        if(startingHour < 0 || startingHour > 23)
            return false;
        if(endingHour < 1 || endingHour > 24)
            return false;
        if(!endingHour.equals(startingHour+1)){
            return false;
        }
        return true;
    }
}
