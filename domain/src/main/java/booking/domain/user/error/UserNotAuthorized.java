package booking.domain.user.error;


import booking.domain.user.UserEmail;

/**
 * This class define an error /Exception can occur if a Business Rule is not respected
 */
public class UserNotAuthorized extends RuntimeException {

    private static final String MESSAGE_TEMPLATE = "User identified by email %s is not Authorized for this operation";

    public UserNotAuthorized(UserEmail email) {
        super(String.format(MESSAGE_TEMPLATE, email.getEmail()));
    }

}