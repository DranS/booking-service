package booking.domain.user.error;

/**
 * This class define an error /Exception can occur if a Business Rule is not respected
 */
public class UserEmailInvalidFormat extends RuntimeException {

    private static final String MESSAGE_TEMPLATE = "User email %s has not a valid format";

    public UserEmailInvalidFormat(String email) {
        super(String.format(MESSAGE_TEMPLATE, email));
    }

}