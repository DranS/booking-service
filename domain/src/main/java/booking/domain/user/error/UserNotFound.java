package booking.domain.user.error;


import booking.domain.user.UserEmail;

/**
 * This class define an error /Exception can occur if a Business Rule is not respected
 */
public class UserNotFound extends RuntimeException {

    private static final String MESSAGE_TEMPLATE = "User identified by email %s does not exist";

    public UserNotFound(UserEmail email) {
        super(String.format(MESSAGE_TEMPLATE, email.getEmail()));
    }

}