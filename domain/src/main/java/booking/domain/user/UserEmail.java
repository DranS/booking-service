package booking.domain.user;

import booking.domain.user.error.UserEmailInvalidFormat;
import lombok.Data;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.io.Serializable;

/**
 * ValueObject class which define a UserEmail object
 */
@Data
public final class UserEmail implements Serializable {

    private String email;

    public UserEmail(String email) {
        if(!isValidEmailAddress(email))
            throw new UserEmailInvalidFormat(email);

        this.email = email;
    }

    public static boolean isValidEmailAddress(String email) {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }

}
