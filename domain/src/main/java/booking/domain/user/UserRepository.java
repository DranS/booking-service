package booking.domain.user;


import java.util.Optional;

/**
 * Interface which define method use in different usecase
 * This interface can be use to easily change the implementation of the repository without to change the usecase
 */
public interface UserRepository {

    Optional<User> findByEmail(UserEmail email);

}
