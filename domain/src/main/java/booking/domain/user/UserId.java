package booking.domain.user;


import booking.domain.common.uuid.ValueObjectUUID;

import java.io.Serializable;
import java.util.UUID;

/**
 * ValueObject class which define a UserId object
 */
public final class UserId extends ValueObjectUUID implements Serializable {

    public UserId() {
        super();
    }

    public UserId(String uuid) {
        super(uuid);
    }

    public UserId(UUID uuid) {
        super(uuid);
    }
}
