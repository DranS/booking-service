package booking.domain.user;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * ValueObject class which define a UserName object
 */
@Data
@AllArgsConstructor
public class UserName {

    private String name;

    @Override
    public String toString(){
        return name;
    }
}
