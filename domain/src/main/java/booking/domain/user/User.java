package booking.domain.user;

import lombok.Builder;
import lombok.Data;

/**
 * Entity class which define a User object
 */
@Data
@Builder
public class User {

    public static final String AGGREGATE_TYPE = "User";

    private UserId userId;
    private UserEmail userEmail;
    private UserName name;

}
