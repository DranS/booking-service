package booking.domain.room.error;


/**
 * This class define an error /Exception can occur if a Business Rule is not respected
 */
public class MeetingRoomNotFound extends RuntimeException {

    private static final String MESSAGE_TEMPLATE = "Meeting room identified by %s does not exist";

    public MeetingRoomNotFound(String meetingRoomName) {
        super(String.format(MESSAGE_TEMPLATE, meetingRoomName));
    }

}