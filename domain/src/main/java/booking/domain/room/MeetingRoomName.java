package booking.domain.room;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * ValueObject class which define a MeetingRoomName object
 */
@Data
@AllArgsConstructor
public class MeetingRoomName {

    private String name;

    @Override
    public String toString(){
        return name;
    }
}
