package booking.domain.room;

import lombok.Builder;
import lombok.Data;

/**
 * Entity class which define a Meeting Room object
 */
@Data
@Builder
public class MeetingRoom {

    public static final String AGGREGATE_TYPE = "MeetingRoom";

    private MeetingRoomId meetingRoomId;
    private MeetingRoomName meetingRoomName;
    private String info;

}
