package booking.domain.room;


import booking.domain.common.uuid.ValueObjectUUID;

import java.io.Serializable;
import java.util.UUID;

/**
 * ValueObject class which define a MeetingRoomId object
 */
public final class MeetingRoomId extends ValueObjectUUID implements Serializable {

    public MeetingRoomId() {
        super();
    }

    public MeetingRoomId(String uuid) {
        super(uuid);
    }

    public MeetingRoomId(UUID uuid) {
        super(uuid);
    }
}
