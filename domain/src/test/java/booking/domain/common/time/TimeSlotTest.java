package booking.domain.common.time;

import booking.domain.common.time.error.TimeSlotInvalid;
import org.junit.Assert;
import org.junit.Test;

public class TimeSlotTest {

    @Test
    public void testNominalCase(){
        int startingHour = 0;
        int endingHour = 1;

        nominalCases(startingHour, endingHour);
    }

    @Test
    public void testBoundary_upper_case(){
        int startingHour = 23;
        int endingHour = 24;

        nominalCases(startingHour, endingHour);
    }

    @Test
    public void testBoundary_lower_case(){
        int startingHour = 00;
        int endingHour = 01;

        nominalCases(startingHour, endingHour);
    }

    private void nominalCases(int startingHour, int endingHour){
        TimeSlot timeSlot = new TimeSlot(startingHour, endingHour);

        Assert.assertNotNull(timeSlot);
        Assert.assertEquals(timeSlot.getStartingHour().intValue(), startingHour);
        Assert.assertEquals(timeSlot.getEndingHour().intValue(), endingHour);
    }

    @Test(expected = TimeSlotInvalid.class)
    public void testBusinessRules_startingHour_under_midnight(){
        int startingHour = -2;
        int endingHour = 1;

        new TimeSlot(startingHour, endingHour);
    }

    @Test(expected = TimeSlotInvalid.class)
    public void testBusinessRules_startingHour_equals_endingHour(){
        int startingHour = 1;
        int endingHour = 1;

        new TimeSlot(startingHour, endingHour);
    }

    @Test(expected = TimeSlotInvalid.class)
    public void testBusinessRules_startingHour_and_endingHour_upper_limit(){
        int startingHour = 24;
        int endingHour = 25;

        new TimeSlot(startingHour, endingHour);
    }

    @Test(expected = TimeSlotInvalid.class)
    public void testBusinessRules_endingHour_and_below_limit(){
        int startingHour = 01;
        int endingHour = 00;

        new TimeSlot(startingHour, endingHour);
    }
}