# build stage
FROM gradle:alpine AS build-env
USER root
RUN mkdir app
ADD . /app
WORKDIR /app
RUN gradle assemble --stacktrace --info

# final stage
FROM openjdk:8-jre
COPY --from=build-env /app/boot/build/libs/booking-service.jar app.jar
ENTRYPOINT [ "sh", "-c", "java -Djava.security.egd=file:/dev/./urandom -jar app.jar --server.port=8080" ]
EXPOSE 8080

